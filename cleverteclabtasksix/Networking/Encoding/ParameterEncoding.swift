//
//  ParameterEncoding.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 17.10.21.
//

import Foundation

public typealias Parameters = [String: String]

public protocol ParameterEncoding {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}
