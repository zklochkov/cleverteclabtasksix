//
//  HTATMEndPoint.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 17.10.21.
//

import Foundation

enum NetworkEnvironment {
    case atm
}

public enum Api {
    case atm
    case atmcity(city:String)
    case infobox(city:String)
    case filial(city:String)
}

extension Api: EndPointType {
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .atm: return "https://belarusbank.by/api"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured") }
        print(url)
        return url
    }
    
    var path: String {
        switch self {
        case .filial(let city):
            return "filials_info?city=\(city)"
        case .infobox(let city):
            return "infobox?city=\(city)"
        case .atmcity(let city):
            return "atm?city=\(city)"
        case .atm:
            return "atm"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .atmcity:
            return .requestParameters(bodyParameters: nil, urlParameters: ["city": "city"])
        default:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
