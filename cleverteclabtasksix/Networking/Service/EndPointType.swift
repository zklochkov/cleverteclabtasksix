//
//  EndPointType.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 17.10.21.
//

import Foundation

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
    
}
