//
//  ViewController.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 17.10.21.
//

import UIKit
import MapKit

class MainViewController: UIViewController, MKMapViewDelegate {
    
    private var networkManager: NetworkManager!
    private var langCenter = 52.42505
    private var longCenter = 31.01407
    private let distanceSpan: CLLocationDegrees = 10000
    private let annotation = MKPointAnnotation()
    private var location: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var pointArr: [PointViewModel] = [PointViewModel]()
    var viewModel = HTATMViewModel()
    
    private lazy var mapView: MKMapView = {
        var mp = MKMapView()
        mp.mapType = MKMapType.standard
        mp.isZoomEnabled = true
        mp.isScrollEnabled = true
        
        return mp
    }()
    
    init(networkManager: NetworkManager) {
        super.init(nibName: nil, bundle: nil)
        self.networkManager = networkManager
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftMargin:CGFloat = 10
        let topMargin:CGFloat = 10
        
        mapView.frame = CGRect(x: leftMargin, y: topMargin, width: view.frame.size.width, height: view.frame.size.height)
        mapView.center = view.center
        mapView.delegate = self
        
        view.addSubview(mapView)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.addPointToMap(arrayPoints: self.viewModel.getAMT(city: "Гомель"))
            self.addPointToMap(arrayPoints: self.viewModel.getInfobox(city: "Гомель"))
            self.addPointToMap(arrayPoints: self.viewModel.getFilial(city: "Гомель"))
        }
    }
    
    func addPointToMap(arrayPoints: [PointViewModel]){
        for point in getNArrayElement(array: arrayPoints, count: 10) {
            guard let lang = Double(point.x) else { return }
            guard let long = Double(point.y) else { return }
            self.addAnnotation(lang: lang.rounded(toPlaces: 5), long: long.rounded(toPlaces: 5), title: point.name)
        }
    }
    
    func setCenterMap(lang: Double, long: Double){
        location = CLLocationCoordinate2DMake(lang, long)
        let coordinateRegion = MKCoordinateRegion(center: location, latitudinalMeters: distanceSpan, longitudinalMeters: distanceSpan)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotation(lang: CLLocationDegrees, long: CLLocationDegrees, title: String) {
        let pin = MKPlacemark(coordinate: CLLocationCoordinate2DMake(lang, long))
        let coordinateRegion = MKCoordinateRegion(center: pin.coordinate, latitudinalMeters: distanceSpan, longitudinalMeters: distanceSpan)
        mapView.setRegion(coordinateRegion, animated: true)
        annotation.title = title
        mapView.addAnnotation(annotation)
        mapView.addAnnotation(pin)
    }
    
    func getDistanceToCenter(x: String, y: String) -> Double {
        return sqrt(pow(Double(x) ?? 0.0-langCenter, 2) + pow(Double(y) ?? 0.0-longCenter, 2))
    }
    
    func getNArrayElement(array: [PointViewModel], count: Int) -> [PointViewModel] {
        var arrTemp:[PointViewModel] = [PointViewModel]()
        for item in array[0..<count] {
            arrTemp.append(item)
        }
        return arrTemp
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
