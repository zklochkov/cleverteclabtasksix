//
//  HTPointVIewModel.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 18.10.21.
//

import Foundation
import Combine

public class PointViewModel {
    private let point: Point
    
    private var langCenter = 52.42505
    private var longCenter = 31.01407
    
    init(point: Point){
        self.point = point
    }
    
    public var name: String {
        return point.name
    }
    
    public var x: String {
        return point.x
    }
    
    public var y: String {
        return point.y
    }
    
    public var distanceToCenter: Double {
        return getDistanceToCenter(x: point.x, y: point.y)
    }
    
    func getDistanceToCenter(x: String, y: String) -> Double {
        return sqrt(pow(Double(x) ?? 0.0-langCenter, 2) + pow(Double(y) ?? 0.0-longCenter, 2))
    }
}
//
//class ATMApi {
//    static let shared = ATMApi()
//    private let baseURL = "https://belarusbank.by/api/atm"
//
//    private func absoluteURL(city: String) -> URL? {
//        let queryURL = URL(string: baseURL)!
//        let components = URLComponents(url: queryURL, resolvingAgainstBaseURL: true)
//        guard var urlComponents = components else { return nil }
//        urlComponents.queryItems = [URLQueryItem(name: "city", value: city)]
//        return urlComponents.url
//    }
//
//    func fetchATMInfo(for city: String) -> AnyPublisher<ATM,Never> {
//        guard let url = absoluteURL(city: city) else {
//            return Just(ATM.placeholder).eraseToAnyPublisher()
//        }
//        return URLSession.shared.dataTaskPublisher(for: url)
//            .map { $0.data }
//            .decode(type: ATM.self, decoder: JSONEncoder())
//            .catch { error in Just(ATM.placeholder) }
//            .receive(on: RunLoop.main)
//            .eraseToAnyPublisher()
//    }
//}
