//
//  ATMViewModel.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 18.10.21.
//

import Foundation
import UIKit

class HTATMViewModel {
    private var networkManager: NetworkManager = NetworkManager()
    var point: Point!
    var pointVM: PointViewModel!
    var pointArr: [PointViewModel] = [PointViewModel]()
    
    func getAMT(city:String) -> [PointViewModel] {
        self.networkManager.getAllATM(city: city) {
            data, error in
            if let error = error {
                print(error)
            }
            if let data = data {
                for pos in data {
                    self.point = Point(name: "ATM", x: pos.gps_x, y: pos.gps_y)
                    self.pointVM = PointViewModel(point: self.point)
                    self.pointArr.append(self.pointVM)
                }
            }
        }
        return pointArr
    }
    
    func getInfobox(city:String) -> [PointViewModel] {
        self.networkManager.getAllInfobox(city:"Гомель") {
            data, error in
            if let error = error {
                print(error)
            }
            if let data = data {
                for pos in data {
                    self.point = Point(name: "Infobox", x: pos.gps_x, y: pos.gps_y)
                    self.pointVM = PointViewModel(point: self.point)
                    self.pointArr.append(self.pointVM)
                }
            }
        }
        return pointArr
    }
    
    func getFilial(city:String) -> [PointViewModel] {
        self.networkManager.getAllFilial(city:"Гомель") {
            data, error in
            if let error = error {
                print(error)
            }
            if let data = data {
                for pos in data {
                    self.point = Point(name: "Filial", x: pos.GPS_X, y: pos.GPS_Y)
                    self.pointVM = PointViewModel(point: self.point)
                    self.pointArr.append(self.pointVM)
                }
            }
        }
        return pointArr
    }
}
