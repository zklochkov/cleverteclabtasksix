//
//  Double+Ex.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 17.10.21.
//

import Foundation

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
