//
//  HTPointModel.swift
//  cleverteclabtasksix
//
//  Created by Zakhar Klochkov on 17.10.21.
//

import Foundation

class Point {
    public let name: String
    public let x: String
    public let y: String
//    var distanceToCenter: Double
    
    public init(name:String,
                x:String,
                y:String
//                distanceToCenter: Double
    ) {
        self.name = name
        self.x = x
        self.y = y
//        self.distanceToCenter = distanceToCenter
    }
}
